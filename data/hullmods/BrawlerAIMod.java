package data.hullmods;

import com.fs.starfarer.api.combat.ShipAPI;

public class BrawlerAIMod extends BaseHullMod {
    
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (
                !ship.getVariant().getHullMods().contains("carrier_ai") 
                && !ship.getVariant().getHullMods().contains("sniper_ai") 
                );
    }
}
