package data.hullmods;

import com.fs.starfarer.api.combat.ShipAPI;

public class CarrierAIMod extends BaseHullMod {
  
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (
                !ship.getVariant().getHullMods().contains("brawler_ai") 
                && !ship.getVariant().getHullMods().contains("sniper_ai") 
                );
    }    
    
}
