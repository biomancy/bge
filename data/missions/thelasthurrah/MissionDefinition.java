package data.missions.thelasthurrah;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "ESS", FleetGoal.ATTACK, false, 10);
		api.initFleet(FleetSide.ENEMY, "HSS", FleetGoal.ATTACK, true, 10);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "Exar Secundus navy with heavy fighter complement");
		api.setFleetTagline(FleetSide.ENEMY, "Hegemony security detachment under Commodore Jensulte");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("Defeat all enemy forces");
		api.addBriefingItem("ESS Indomitable must survive");
		api.addBriefingItem("Keep the Gemini on the field to repair & refit your fighters");
		api.addBriefingItem("Capturing all objectives stops enemy reinforcements");
		
		// Set up the player's fleet
                api.addToFleet(FleetSide.PLAYER, "bge_waechter_standard", FleetMemberType.SHIP, "ISS Black Star", true, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.PLAYER, "bge_koenig_siege", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_koenig_standard", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_arbiter_standard", FleetMemberType.SHIP, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_arbiter_siege", FleetMemberType.SHIP, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_zophe_siege", FleetMemberType.SHIP, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_waechter_siege", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_zophe_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_flusskrebs_siege", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_flusskrebs_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_vergifter_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_vergifter_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_hornisse_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_hornisse_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_nymphe_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_nymphe_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_krieger_standard", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN).getCaptain().setPersonality("suicidal");
		
		// Mark player flagship as essential
		//api.defeatOnShipLoss("ESS Indomitable");
		
		// Set up the enemy fleet
		api.addToFleet(FleetSide.ENEMY, "onslaught_Standard", FleetMemberType.SHIP, false);
//		api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, false);
//		api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "dominator_Support", FleetMemberType.SHIP, false);
		//api.addToFleet(FleetSide.ENEMY, "dominator_Support", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "enforcer_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "hound_Assault", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
		
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		//api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		//api.addToFleet(FleetSide.ENEMY, "talon_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "broadsword_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "piranha_wing", FleetMemberType.FIGHTER_WING, false);
		
		
		// Set up the map.
		float width = 24000f;
		float height = 18000f;
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		for (int i = 0; i < 15; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 100f + (float) Math.random() * 900f; 
			api.addNebula(x, y, radius);
		}
		
		api.addNebula(minX + width * 0.8f - 1000, minY + height * 0.4f, 2000);
		api.addNebula(minX + width * 0.8f - 1000, minY + height * 0.5f, 2000);
		api.addNebula(minX + width * 0.8f - 1000, minY + height * 0.6f, 2000);
		
		api.addObjective(minX + width * 0.8f - 1000, minY + height * 0.4f, "nav_buoy");
		api.addObjective(minX + width * 0.8f - 1000, minY + height * 0.6f, "nav_buoy");
		api.addObjective(minX + width * 0.3f + 1000, minY + height * 0.3f, "comm_relay");
		api.addObjective(minX + width * 0.3f + 1000, minY + height * 0.7f, "comm_relay");
		api.addObjective(minX + width * 0.5f, minY + height * 0.5f, "sensor_array");
		api.addObjective(minX + width * 0.2f + 1000, minY + height * 0.5f, "sensor_array");
		
		// Add an asteroid field
		api.addAsteroidField(minX + width * 0.3f, minY, 90, 3000f,
								20f, 70f, 50);
		
		// Add some planets.  These are defined in data/config/planets.json.
		api.addPlanet(0, 0, 200f, "terran", 350f, true);
	}

}






