package data.missions.direstraits;

import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.combat.EscapeRevealPlugin;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "HSS", FleetGoal.ESCAPE, false, 5);
		api.initFleet(FleetSide.ENEMY, "TTS", FleetGoal.ATTACK, true, 5);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "Hegemony relief fleet with mercenary escort");
		api.setFleetTagline(FleetSide.ENEMY, "Tri-Tachyon containment task force");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("ISS Black Star must survive");
		api.addBriefingItem("At least 25% of the Hegemony forces must escape");
		
		
		// Set up the player's fleet
		api.addToFleet(FleetSide.PLAYER, "bge_waechter_standard", FleetMemberType.SHIP, "ISS Black Star", true, CrewXPLevel.ELITE);
		api.addToFleet(FleetSide.PLAYER, "bge_koenig_standard", FleetMemberType.SHIP, false, CrewXPLevel.VETERAN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_arbiter_standard", FleetMemberType.SHIP, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_zophe_standard", FleetMemberType.SHIP, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_waechter_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_zophe_standard", FleetMemberType.SHIP, false, CrewXPLevel.GREEN).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_hornisse_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_hornisse_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_nymphe_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");
		api.addToFleet(FleetSide.PLAYER, "bge_nymphe_standard_wing", FleetMemberType.FIGHTER_WING, false).getCaptain().setPersonality("suicidal");


		
		// Mark player flagship as essential
		api.defeatOnShipLoss("ISS Black Star");
		
		// Set up the enemy fleet
		api.addToFleet(FleetSide.ENEMY, "astral_Elite", FleetMemberType.SHIP, "TTS Ephemeral", false);
		api.addToFleet(FleetSide.ENEMY, "medusa_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "medusa_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "sunder_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "wolf_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "wolf_CS", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);
		api.addToFleet(FleetSide.ENEMY, "omen_PD", FleetMemberType.SHIP, false);
		
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "wasp_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "xyphos_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "xyphos_wing", FleetMemberType.FIGHTER_WING, false);
		api.addToFleet(FleetSide.ENEMY, "dagger_wing", FleetMemberType.FIGHTER_WING, false);
		
		
		// Set up the map.
		float width = 18000f;
		float height = 24000f;
		
		api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);
		
		float minX = -width/2;
		float minY = -height/2;
		
		for (int i = 0; i < 15; i++) {
			float x = (float) Math.random() * width - width/2;
			float y = (float) Math.random() * height - height/2;
			float radius = 100f + (float) Math.random() * 900f; 
			api.addNebula(x, y, radius);
		}
		
		api.addNebula(minX + width * 0.8f, minY + height * 0.4f, 2000);
		api.addNebula(minX + width * 0.8f, minY + height * 0.5f, 2000);
		api.addNebula(minX + width * 0.8f, minY + height * 0.6f, 2000);
		
		api.addObjective(minX + width * 0.8f, minY + height * 0.4f, "sensor_array");
		api.addObjective(minX + width * 0.8f, minY + height * 0.6f, "nav_buoy");
		api.addObjective(minX + width * 0.3f, minY + height * 0.3f, "nav_buoy");
		api.addObjective(minX + width * 0.3f, minY + height * 0.7f, "sensor_array");
		api.addObjective(minX + width * 0.2f, minY + height * 0.5f, "comm_relay");

		
		api.addAsteroidField(minX + width * 0.5f, minY + height, 270, width,
								20f, 70f, 50);
		
		
		api.addPlanet(0, 0, 200f, "barren", 0f, true);
		
		
		BattleCreationContext context = new BattleCreationContext(null, null, null, null);
		context.setInitialEscapeRange(7000f);
		api.addPlugin(new EscapeRevealPlugin(context));
	}

}






