package data.scripts.shipai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.combat.CombatAssignmentType;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI;
import com.fs.starfarer.api.combat.CombatFleetManagerAPI.AssignmentInfo;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;

import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;

import org.lwjgl.util.vector.Vector2f;

//Bomber AI
//Attempts to run up on targets and attack them, then leave.
public class BomberAI implements ShipAIPlugin
{
    // Our ship object
    private final ShipAPI ship;
    
    //Our System AI
    private final ShipSystemAIScript script;
    //Phase Cloak uses its own sub-AI for convenience.
    private final ShipSystemAIScript cloakScript;

    // Our current target (can be null)
    private ShipAPI target = null;
    
    //Booleans set current control state, updated every tick
    private boolean isAccelerating = false;
    private boolean turnRight = false;
    private boolean turnLeft = false;
    //Used for steering decisions
    private Vector2f destination = null;
    private Vector2f destinationVel = null;

    
    private final FleetSide side;
    //private CombatFleetManagerAPI fleetManager;
    //private CombatEngineAPI engine = Global.getCombatEngine();
    
    private final IntervalUtil checkInterval = new IntervalUtil(0.05f, 0.15f);
    //longInterval causes a new evalutation of targets regardless of anything else, to prevent AIs getting stuck stupidly.  
    //Warning; don't run this too often, otherwise the AI won't even aim the guns properly.
    private final IntervalUtil longInterval = new IntervalUtil(10.0f, 15.1f);
    private boolean doneOnce = false;
    
    //Skip-bombing control variables
    private boolean isRunning = false;
    private boolean canRun = false;
    private final IntervalUtil runInterval = new IntervalUtil(5.0f, 10.1f);
    
    private float strafeDistance;
    private boolean strafeRight;
    private boolean strafeLeft;
                
    public BomberAI (ShipAPI ship, ShipSystemAIScript script, ShipSystemAIScript cloakScript, boolean shouldStrafe, float strafeDistance)
    {
        this.ship = ship;
        if(ship.getOwner() == 0){
            this.side = FleetSide.PLAYER;
        } else {
            this.side = FleetSide.ENEMY;
        }
        this.strafeDistance = strafeDistance;
        this.script = script;
        if(script != null){
            this.script.init(ship,null,null,Global.getCombatEngine());
        }
        this.cloakScript = cloakScript;
        if(cloakScript != null){
            this.cloakScript.init(ship,null,null,Global.getCombatEngine());
        }
    }
    
    //This is our sorter, that finds the closest ShipAPI that is not a Drone.
    public ShipAPI findBestTarget(ShipAPI ship) 
    {
        //getting what you want to search through
        ArrayList<ShipAPI> possibleTarget = new ArrayList<ShipAPI>();
        //lazylib is convenient, gets everything you want in Searchrange     
        List nearbyEnemies = AIUtils.getNearbyEnemies(ship, 7000f);
        Collections.sort(nearbyEnemies,new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));
        
        //filters through the ship list and selects them by class  
        //Civilian and Carrier ships are top targets
        for(Iterator iterMe = nearbyEnemies.iterator(); iterMe.hasNext(); )
        {
            ShipAPI enemy = (ShipAPI) iterMe.next();
            //criteria to add to list (you can put anything that will return a true/false here)
            if ((enemy.getHullSpec().getHints().contains(ShipTypeHints.CIVILIAN) 
                    || enemy.getHullSpec().getHints().contains(ShipTypeHints.CARRIER)) 
                    && !enemy.isHulk() && Global.getCombatEngine().isEntityInPlay(enemy))
            {
                if(enemy.getPhaseCloak() != null){
                    if(!enemy.getPhaseCloak().isOn() || !enemy.getPhaseCloak().isActive()){
                        possibleTarget.add(enemy);  //add to possibles
                    }
                } else {
                     possibleTarget.add(enemy);  //add to possibles
                }
            }
        }         
        //Cruisers second
        for(Iterator iterMe = nearbyEnemies.iterator(); iterMe.hasNext(); )
        {
            ShipAPI enemy = (ShipAPI) iterMe.next();
            //criteria to add to list (you can put anything that will return a true/false here)
            if (enemy.isCruiser())
            {
                if(enemy.getPhaseCloak() != null){
                    if(!enemy.getPhaseCloak().isOn() || !enemy.getPhaseCloak().isActive()){
                        possibleTarget.add(enemy);  //add to possibles
                    }
                } else {
                     possibleTarget.add(enemy);  //add to possibles
                }
            }
        }    
        //Destroyers third
         for(Iterator iterMe = nearbyEnemies.iterator(); iterMe.hasNext(); )
        {
            ShipAPI enemy = (ShipAPI) iterMe.next();
            //criteria to add to list (you can put anything that will return a true/false here)
            if (enemy.isDestroyer())
            {
                if(enemy.getPhaseCloak() != null){
                    if(!enemy.getPhaseCloak().isOn() || !enemy.getPhaseCloak().isActive()){
                        possibleTarget.add(enemy);  //add to possibles
                    }
                } else {
                     possibleTarget.add(enemy);  //add to possibles
                }
            }
        }
         //Add in everything else, including Phased ships, but NOT DRONES.
         for(Iterator iterMe = nearbyEnemies.iterator(); iterMe.hasNext(); )
        {
            ShipAPI enemy = (ShipAPI) iterMe.next();
            if(!enemy.isDrone()){
                //criteria to add to list (you can put anything that will return a true/false here)
                possibleTarget.add(enemy);  //add to possibles
            }
        }
         
        //Still no target?  Cheat!
        if(possibleTarget.isEmpty()){
            nearbyEnemies = Global.getCombatEngine().getShips();
            Collections.sort(nearbyEnemies,new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));
            for(Iterator iterMe = nearbyEnemies.iterator(); iterMe.hasNext(); )
            {
                ShipAPI enemy = (ShipAPI) iterMe.next();
                //criteria to add to list (you can put anything that will return a true/false here)
                if(enemy.getOwner() != ship.getOwner()){
                    possibleTarget.add(enemy);  //add to possibles
                }
            }
        }
        
        //If we haven't found a target within the search arc, this returns the closest enemy to this ship, or null
        if(!possibleTarget.isEmpty()){
            canRun = !(possibleTarget.get(0).isFighter()) && !(possibleTarget.get(0).isDrone());
            return possibleTarget.get(0);
        }else{
            return null;
        }
    }

     //Main logic
    @Override
    public void advance(float amount)
    {
        //LOGIC CHECKS
        if(Global.getCombatEngine() == null) return;
        if (Global.getCombatEngine().isPaused()) return;

        //One-time setup for autofire code.
        if(doneOnce = false){
            //WEAPON CONTROL
           //If weapon groups aren't on autofire, put them on autofire
            if(ship.getVariant().getWeaponGroups().size() > 0){
            if(!ship.getVariant().getWeaponGroups().get(0).isAutofireOnByDefault()){
                ship.getVariant().getWeaponGroups().get(0).setAutofireOnByDefault(true);
            }
            }
            if(ship.getVariant().getWeaponGroups().size() > 1){
            if(!ship.getVariant().getWeaponGroups().get(1).isAutofireOnByDefault()){
                ship.getVariant().getWeaponGroups().get(1).setAutofireOnByDefault(true);
            }
            }
            if(ship.getVariant().getWeaponGroups().size() > 2){
            if(!ship.getVariant().getWeaponGroups().get(2).isAutofireOnByDefault()){
                ship.getVariant().getWeaponGroups().get(2).setAutofireOnByDefault(true);
            } 
            }
            if(ship.getVariant().getWeaponGroups().size() > 3){
            if(!ship.getVariant().getWeaponGroups().get(3).isAutofireOnByDefault()){
                ship.getVariant().getWeaponGroups().get(3).setAutofireOnByDefault(true);
            }
            }
            if(ship.getVariant().getWeaponGroups().size() > 4){
             if(!ship.getVariant().getWeaponGroups().get(4).isAutofireOnByDefault()){
                ship.getVariant().getWeaponGroups().get(4).setAutofireOnByDefault(true);
            }
            }  
            doneOnce = true;
        }        
        
        //If the engine's not paused, tick tock
        checkInterval.advance(amount);   
        longInterval.advance(amount);
        runInterval.advance(amount);
        
        //Turns off the running cycle.
        if(runInterval.intervalElapsed()){
            if(isRunning) isRunning = false;
        }
        
        //If enough time has passed, re-evaluate our best target, reset our weapon states, and randomize the interval.
        if(longInterval.intervalElapsed()){
            //Refresh the target
            target = findBestTarget(this.ship);
            strafeDistance = bestStrafeDistance(this.ship);
            longInterval.randomize();
        }
        
        //CONTINUAL CONTROL STATES
        //Checks booleans and gives continual commands here.
        //Done here so that we can use IntervalUtil to save CPU later
        if(isAccelerating){
            ship.giveCommand(ShipCommand.ACCELERATE,null,0);
        }else{
            ship.giveCommand(ShipCommand.ACCELERATE_BACKWARDS,null,0);
        }     
        //Turning and strafing.
        if(turnRight){
            ship.giveCommand(ShipCommand.TURN_RIGHT,null,0);
        }
        if (turnLeft){
            ship.giveCommand(ShipCommand.TURN_LEFT,null,0);
        }
        if(strafeRight){
            ship.giveCommand(ShipCommand.STRAFE_RIGHT,null,0);
        }
        if(strafeLeft){
            ship.giveCommand(ShipCommand.STRAFE_LEFT,null,0);
        }               

        //Calls the ship's System AI and the Cloak AI and lets it do its thing, whatever that is.
        if(script != null){
            script.advance(amount,destination,destination,target);
        }
        if(cloakScript != null){
            cloakScript.advance(amount,destination,destination,target);
        }
            


        //MAIN AI LOGIC
        //This code governs the ship's behavior and outputs boolean states that...
        //...are used in the continual control state code.
        //By using an IntervalUtil here, we can do practical load-balancing at the cost of accuracy.
        if(checkInterval.intervalElapsed()){
            //FLUX CONTROL
            //If Flux is greater than 75% of max, Vent if possible.
            if((ship.getFluxTracker().getHardFlux() >= ship.getFluxTracker().getMaxFlux() * 0.75f) 
            || (ship.getFluxTracker().getCurrFlux() >= ship.getFluxTracker().getMaxFlux() * 0.9f))
            {
                ship.giveCommand(ShipCommand.VENT_FLUX,null,0);
            }
            
            //SHIELD CONTROL
            //If shields are down, raise them.
            //Also points the shields at the best possible target, and lowers them if it's going to overload if hit
            if(ship.getShield() != null){
                if(ship.getShield().isOff()){
                    ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK,null,0);
                }else{   
                    ShipAPI nearestEnemy = AIUtils.getNearestEnemy(ship);
                    float possDamage = 0f;
                    List nearbyProjectiles = CombatUtils.getProjectilesWithinRange(ship.getLocation(),200f + ship.getCollisionRadius());
                    
                    for(Iterator iterMe = nearbyProjectiles.iterator(); iterMe.hasNext(); )
                    {
                        DamagingProjectileAPI shot = (DamagingProjectileAPI) iterMe.next();
                        if(shot.getOwner() != ship.getOwner()){
                            possDamage += (shot.getDamageAmount() + shot.getEmpAmount()) * ship.getMutableStats().getShieldDamageTakenMult().getModifiedValue();
                        }
                    }
                   if(possDamage + ship.getFluxTracker().getHardFlux() >= ship.getFluxTracker().getMaxFlux()){
                       ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK,null,0);
                   }
                    
                    if(nearestEnemy != null && nearestEnemy.isAlive() && !nearestEnemy.isHulk()){
                        Vector2f loc = nearestEnemy.getLocation();
                        ship.getMouseTarget().set(loc.getX(), loc.getY());
                    } else if (target != null) {
                        ship.getMouseTarget().set(target.getLocation().getX(),target.getLocation().getY());
                    } else if (destination != null) {
                        ship.getMouseTarget().set(destination.getX(),destination.getY());
                    }  else {    
                        ship.getMouseTarget().set(ship.getLocation().getX(),ship.getLocation().getY());
                    }
                }
            }
            
            //UPDATE TARGETS
            //If we don't have a target, find one.   
            //If our target is dead, removed from the sim or has switched sides, find a new target.
            if (        target == null // unset 
                    ||  target.isHulk()
                    ||  (ship.getOwner() == target.getOwner())        // friendly
                    ||  !Global.getCombatEngine().isEntityInPlay(target) // completely removed
                    )
            {
                target = null;
                target = findBestTarget(ship);
                if(target != null){ 
                    ship.setShipTarget(target);
                } else {
                    ship.setShipTarget(null);
                }
            } 
            //Destination MUST be reset to null at this stage of the state machine!
            destination = null;
            destinationVel = null;
            
            //DESTINATION CONTROL
            //Does this ship have a destination of some kind?  Yes?  Good, go there!
            CombatFleetManagerAPI fleetManager = Global.getCombatEngine().getFleetManager(ship.getOwner());
            if(fleetManager != null){
                AssignmentInfo info = fleetManager.getAssignmentFor(ship);
                CombatAssignmentType assignmentType;
                if(info != null){
                    assignmentType = info.getType();
                    if(assignmentType == null){
                       destination = null;
                       destinationVel = null;
                    }
                    if(assignmentType == CombatAssignmentType.SEARCH_AND_DESTROY){
                        destination = null;
                        destinationVel = null;
                    } else if (assignmentType == CombatAssignmentType.RETREAT){
                       BattleCreationContext context =  Global.getCombatEngine().getContext();
                       FleetGoal playerGoal = context.getPlayerGoal();
                       FleetGoal otherGoal = context.getOtherGoal();
                       if(ship.getOwner() == 0){
                           if(AIData.battleType == 1 || playerGoal == FleetGoal.ESCAPE){
                                destination = new Vector2f(ship.getLocation().getX(),ship.getLocation().getY() + 50000f);
                                destinationVel = new Vector2f();
                           }else {
                                destination = new Vector2f(ship.getLocation().getX(),ship.getLocation().getY() - 50000f);
                                destinationVel = new Vector2f();
                           }
                       } else {
                           if(AIData.battleType == 1 || otherGoal == FleetGoal.ESCAPE){
                                destination = new Vector2f(ship.getLocation().getX(),ship.getLocation().getY() + 50000f);
                                destinationVel = new Vector2f();
                           }else {
                                destination = new Vector2f(ship.getLocation().getX(),ship.getLocation().getY() + 50000f);
                                destinationVel = new Vector2f();
                           }
                       }
                    } else if (assignmentType == CombatAssignmentType.ENGAGE 
                                || assignmentType == CombatAssignmentType.HARASS 
                                || assignmentType == CombatAssignmentType.INTERCEPT
                                || assignmentType == CombatAssignmentType.STRIKE
                            ){
                        //If given a direct order to go kill a Thing, go kill that Thing, do not pass Go, do not collect $200.
                        destination = info.getTarget().getLocation();
                        destinationVel = info.getTarget().getVelocity();
                        if (destination == null || destinationVel == null){
                            return;
                        }
                        List closeBy = CombatUtils.getShipsWithinRange(destination,50f);
                        if(!closeBy.isEmpty()){
                            target = (ShipAPI) closeBy.get(0);  
                        }
                    } else if(info.getTarget() != null){
                        destination = info.getTarget().getLocation();
                        destinationVel = info.getTarget().getVelocity();
                    } else{
                        destination = null;
                        destinationVel = null;
                    }
                }
            }

            //Sets a combat destination using a randomized circle for attacking distant targets who aren't too fast for this to matter
            //The two distance settings (for the outside distance and the circle position) are crucial and a bit picky!
            if(target != null && !target.isFighter() && !target.isDrone() && target.isAlive() && !target.isHulk() && destination == null && MathUtils.getDistance(ship,target) > 3000f){
                destination = MathUtils.getRandomPointOnCircumference(target.getLocation(),250f);
                destinationVel = new Vector2f(0f,0f);
            }

            //Now steer, and kite, if that's appropriate.
            if (target == null && destination == null){
                steerMe(new Vector2f(0f,0f),new Vector2f(0f,0f),false);
            } else if (target!=null && destination == null) //if you have a target, but not a destination
            {           
                steerMe(target.getLocation(),target.getVelocity(),!target.isDrone() && !target.isFighter() && target.isAlive() && !target.isHulk());
            } else if (destination != null && target == null){
                steerMe(destination,destinationVel,false);
            } else if (target != null && destination != null && MathUtils.getDistance(ship.getLocation(),target.getLocation()) < 1500f){
                steerMe(target.getLocation(),target.getVelocity(),!target.isDrone() && !target.isFighter() && target.isAlive() && !target.isHulk());
            } else if (destination != null) {
                steerMe(destination,destinationVel,false);      
            } else if (target != null){
                steerMe(target.getLocation(),target.getVelocity(),!target.isDrone() && !target.isFighter() && target.isAlive() && !target.isHulk());                
            } else {
                //If all else fails, wander around the center of the map
                steerMe(MathUtils.getRandomPointInCircle(new Vector2f(0f,0f),2000f),new Vector2f(0f,0f),false);
            }
        }
    }
    
    private void steerMe(Vector2f TLoc, Vector2f TVel, boolean kiteable){
      //Get ship's velocity and location
      Vector2f MVel = ship.getVelocity();
      Vector2f MLoc = ship.getLocation();
      float MFace = ship.getFacing();

      //Testing InterceptPoint, tests for slow-moving things now
      Vector2f TLead;
      if(TVel.length() < 300f){
          TLead = TLoc;
      } else {
          if(target == null){
              TLead = TLoc;
          } else {
            float TCol = target.getCollisionRadius();
            Vector2f TVelShort = VectorDirection(TVel, TCol);
            TLead = new Vector2f(TLoc.getX() + TVelShort.getX(),TLoc.getY() + TVelShort.getY());
          }
      }

      //Main aiming logic
      float AngleToEnemy = VectorUtils.getAngle(MLoc, TLead);
      float angleToTarget = getAngleDifference(MFace, AngleToEnemy);         

      float AbsAngD = Math.abs(angleToTarget);
      
     
      //SKIP-BOMBING ROUTINE.  If close enough to target RUN AWAY      


      if(!isRunning){
        //Are we close to the target?  Time to run away on the next frame
       if((AbsAngD < 3 && canRun && MathUtils.getDistance(TLoc,MLoc) <= strafeDistance / 1.25f) && !isRunning){
           isRunning = true;
           runInterval.randomize();//Reset that timer!
       }
       
      //Strafe a little bit to correct our aim.
      if (AbsAngD > 5 && MathUtils.getDistance(TLoc,MLoc) <= strafeDistance)
      {
          //course correct for ship velocity vector (some bugs when severely off target)
          float MFlightAng = VectorUtils.getAngle(new Vector2f(0, 0), MVel);
          float MFlightCC = getAngleDifference(MFace, MFlightAng);
          if (Math.abs(MFlightCC)>20)
          {
              if(MFlightCC < 0){
                  strafeLeft = false;
                  strafeRight = true;
              } else {
                  strafeRight = false;
                  strafeLeft = true;
              }
          }
      }       
          
        //point towards target
        if (AbsAngD >= 0.5)
        {
            if(angleToTarget > 0){
                turnLeft = true;
                turnRight = false;
            }else{
                turnRight = true;
                turnLeft = false;
            }
        }

        //Damp angular velocity if we're getting close to the target angle, to cut down on wiggle
        if(AbsAngD < Math.abs(ship.getAngularVelocity())){
            if(angleToTarget > 0){
                ship.setAngularVelocity(AbsAngD); 
            }else if (angleToTarget < 0){
                ship.setAngularVelocity(-AbsAngD);
            }                
        }
      } else {
        //Run away from this target!
        if (AbsAngD <= 150)
        {
            if(angleToTarget > 0){
                turnRight = true;
                turnLeft = false;
            }else{
                turnLeft = true;
                turnRight = false;
            }
        }
      }
        //Acceleration code; we're a bomber, don't ever back-peddle
        isAccelerating = true;
    }

        //Returns a new vector with original direction but new length
    private Vector2f VectorDirection(Vector2f vec, float size)
    {
        float x = vec.getX();
        float y = vec.getY();

        float lenght =(float) Math.hypot(x, y);
        return new Vector2f(x*size/lenght, y*size/lenght);
    }           

    //Gets angle difference between two angles, returns 0-180 +/-
    private float getAngleDifference(float angle1, float angle2)
    {
        float distance = (angle2 - angle1) + 180f;
        distance = (distance / 360.0f);
        distance = ((distance - (float) Math.floor(distance)) * 360f) - 180f;
        return distance;
    }
    
    //Gets the "best" strafing distance, with a maximum distance that makes some sense in Vacuum.
    private float bestStrafeDistance(ShipAPI ship){
        List<WeaponAPI> weapons = ship.getAllWeapons();
        float bestRange = 800f;
        for (WeaponAPI weapon : weapons) {
            if(weapon.getSpec().getType() != WeaponAPI.WeaponType.DECORATIVE && weapon.getSpec().getType() != WeaponAPI.WeaponType.LAUNCH_BAY){
                    float weaponRange = weapon.getRange();
                if(weapon.getAmmo() == 0 && !weapon.isBeam() && weapon.getFluxCostToFire() == 0){
                    weaponRange = 0f;
                }
                if (weaponRange > bestRange){
                    bestRange = weaponRange;
                }
            }
        }
        if(bestRange > 0f){
            //Use some random noise here so that ships don't always stay out at max kiting distances
            return Math.max(500f,Math.min(bestRange - MathUtils.getRandomNumberInRange(300f,800f),2200f - MathUtils.getRandomNumberInRange(300f,800f)));
        } else {
            //Safety catch-all for suckage
            return 800f;
        }
    }      

    //Can be called outside the AI
    @Override
    public void setDoNotFireDelay(float amount) {
    }

    //Only called when Player puts ship back under AI control
    @Override
    public void forceCircumstanceEvaluation() {
        target = findBestTarget(ship);
    }

    //Only gets set via other scripts here, but can be caught by other AI scripts (carriers or healing drones, for example).
    @Override
    public boolean needsRefit() {
        return false;
    }
}
 