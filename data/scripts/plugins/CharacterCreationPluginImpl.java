package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI.CrewXPLevel;
import com.fs.starfarer.api.characters.CharacterCreationPlugin;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import java.util.ArrayList;
import java.util.List;

public class CharacterCreationPluginImpl implements CharacterCreationPlugin {

	CharacterCreationPlugin handoffPlugin = null;  
	
	public CharacterCreationPluginImpl()  
	{  
		// automatic yield of control pass-through in case of conflict:   
		// Exerelin  
		if(canBeLoaded("data.scripts.world.ExerelinGen" ))  
		{  
			try  
			{  
				handoffPlugin = (CharacterCreationPlugin) Global.getSettings().getScriptClassLoader()  
				  .loadClass( "data.scripts.plugins.ExerelinCharacterCreationPluginImpl" )  
				  .newInstance();  
			}  
			catch( ClassNotFoundException e ) {}  
			catch( InstantiationException e ) {}  
			catch( IllegalAccessException e ) {}  
		}  
	}  
	
	public static class ResponseImpl implements Response {
		private String text;
		public ResponseImpl(String text) {
			this.text = text;
		}
		public String getText() {
			return text;
		}
	}
	
	// Not using an enum for this because Janino doesn't support it.
	// It does, apparently, support using enums defined elsewhere - just can't compile new ones.
	private ResponseImpl SUPPLY_OFFICER = new ResponseImpl("Served as a junior supply officer in an independent system's navy");
	private ResponseImpl GUNNER = new ResponseImpl("Hired out as a gunner on a mercenary ship");
	private ResponseImpl ENGINEER = new ResponseImpl("Found employment as an assistant engineer on an exploration vessel");
	private ResponseImpl COPILOT = new ResponseImpl("Spent time as a co-pilot on a patrol ship in an independent system");
	private ResponseImpl SOMETHING_ELSE_1 = new ResponseImpl("Did something else");
	private ResponseImpl ADJUTANT = new ResponseImpl("Served as an adjutant in the Hegemony Navy");
	private ResponseImpl QUARTERMASTER = new ResponseImpl("Performed the duties of a quartermaster on an independent warship");
	private ResponseImpl HELM = new ResponseImpl("Helmed a patrol ship operating in a backwater system");
	private ResponseImpl COMBAT_ENGINEER = new ResponseImpl("Took over the duties of chief combat engineer during a lengthy campaign");
	private ResponseImpl SOMETHING_ELSE_2 = new ResponseImpl("Did something else");
	private ResponseImpl EASY = new ResponseImpl("Easy");
	private ResponseImpl NORMAL = new ResponseImpl("Normal");
	private ResponseImpl BGE = new ResponseImpl("Yes.");
	private ResponseImpl NOBGE = new ResponseImpl("No. I don't like to be bathed in slime and blood while on board of one of their ships.");
	
	private int stage = 0;
	private boolean easyStart = false;
	private String [] prompts = new String [] {
		"Early in your career, you...",
		"More recently, you...",
		"Did you sign a gene sample collection contract with Biomancy Genetic Enigineering?",
		"Select the starting difficulty",
	};
	
	public String getPrompt() {
		if( handoffPlugin != null )  
		{  
			return handoffPlugin.getPrompt();  
		}  
	
		if (stage < prompts.length) return prompts[stage];
		return null;
	}

	@SuppressWarnings("unchecked")
	public List getResponses() {
		if( handoffPlugin != null )  
		{  
			return handoffPlugin.getResponses();  
		}  
		
		List result = new ArrayList();
		if (stage == 0) {
			result.add(SUPPLY_OFFICER);
			result.add(GUNNER);
			result.add(ENGINEER);
			result.add(COPILOT);
			result.add(SOMETHING_ELSE_1);
		} else if (stage == 1) {
			result.add(ADJUTANT);
			result.add(QUARTERMASTER);
			result.add(HELM);
			result.add(COMBAT_ENGINEER);
			result.add(SOMETHING_ELSE_2);
		} else if (stage == 2) {
			result.add(BGE);
			result.add(NOBGE);
		} else if (stage == 3) {
			result.add(EASY);
			result.add(NORMAL);
		}
		return result;
	}

	
	public void submit(Response response, CharacterCreationData data) {
		if( handoffPlugin != null )  
		{  
			handoffPlugin.submit(response, data);  
			return;  
		}  
	
		if (stage == 0) { // just in case
			data.addStartingShipChoice("shuttle_Attack");
                        data.setStartingLocationName("Corvus");
			data.getStartingCoordinates().set(-2500, -3500);
			
			
			if (Global.getSettings().isDevMode() && false) {
				data.addStartingShipChoice("tempest_Attack");
				data.addStartingFleetMember("odyssey_Balanced", FleetMemberType.SHIP);
				data.addStartingFleetMember("enforcer_Elite", FleetMemberType.SHIP);
				data.addStartingFleetMember("venture_Balanced", FleetMemberType.SHIP);
				data.addStartingFleetMember("crig_Standard", FleetMemberType.SHIP);
				data.addStartingFleetMember("ox_Standard", FleetMemberType.SHIP);
				data.addStartingFleetMember("wasp_wing", FleetMemberType.FIGHTER_WING);
				data.addStartingFleetMember("wasp_wing", FleetMemberType.FIGHTER_WING);
				data.getStartingCargo().getCredits().add(50000f);
				data.getStartingCargo().addFuel(1000);
				data.getStartingCargo().addSupplies(800);
				data.getStartingCargo().addMarines(100);
				data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 800);
			}
		}
		
		stage++;
		
		MutableCharacterStatsAPI stats = data.getPerson().getStats();
		if (response == SUPPLY_OFFICER) {
			stats.increaseAptitude("leadership");
			stats.increaseSkill("fleet_logistics");
			stats.increaseSkill("command_experience");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == GUNNER) {
			stats.increaseAptitude("combat");
			stats.increaseSkill("ordnance_expert");
			stats.increaseSkill("target_analysis");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == ENGINEER) {
			stats.increaseAptitude("technology");
			stats.increaseSkill("field_repairs");
			stats.increaseSkill("mechanical_engineering");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == COPILOT) {
			stats.increaseAptitude("combat");
			stats.increaseSkill("helmsmanship");
			stats.increaseSkill("evasive_action");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == SOMETHING_ELSE_1) {
			stats.addAptitudePoints(1);
			stats.addSkillPoints(2);
			data.getStartingCargo().getCredits().add(1000f);
		} 
		
		else if (response == ADJUTANT) {
			stats.increaseAptitude("leadership");
			stats.increaseSkill("fleet_logistics");
			stats.increaseSkill("advanced_tactics");
			data.addStartingShipChoice("lasher_Starting");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == QUARTERMASTER) {
			stats.increaseAptitude("technology");
			stats.increaseSkill("navigation");
			stats.addSkillPoints(1);
			data.addStartingShipChoice("wolf_Starting");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == HELM) {
			stats.increaseAptitude("combat");
			stats.increaseSkill("helmsmanship");
			stats.increaseSkill("evasive_action");
			data.addStartingShipChoice("vigilance_Starting");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == COMBAT_ENGINEER) {
			stats.increaseAptitude("combat");
			stats.increaseSkill("damage_control");
			stats.increaseSkill("flux_modulation");
			data.addStartingShipChoice("lasher_Starting");
			data.getStartingCargo().getCredits().add(3000f);
		} else if (response == SOMETHING_ELSE_2) {
			stats.addAptitudePoints(1);
			stats.addSkillPoints(2);
			data.addStartingShipChoice("warhound_Starting");
			data.getStartingCargo().getCredits().add(1000f);
		}
		
		else if (response == EASY) {
			easyStart = true;
			
			data.getStartingShipChoices().clear();
			data.clearAdditionalShips();
			data.addStartingShipChoice("medusa_Starting");
			data.addStartingShipChoice("tempest_Starting");
			data.addStartingShipChoice("wolf_Starting");
			data.addStartingShipChoice("bge_waechter_standard");
			data.addStartingShipChoice("lasher_Starting");
			data.addStartingFleetMember("mule_Starting", FleetMemberType.SHIP);
			data.getStartingCargo().addFuel(100);
			data.getStartingCargo().addSupplies(200);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 100);
			data.getStartingCargo().addMarines(25);
			data.getStartingCargo().getCredits().add(10000f);
		} else if (response == NORMAL) {
			
		} else if (response == BGE) {
			data.addStartingShipChoice("bge_vergifter_standard");
                        data.addStartingShipChoice("bge_flusskrebs_standard");
                        data.addStartingShipChoice("bge_krieger_standard");
		} else if (response == NOBGE) {
		}
	}

	public void startingShipPicked(String variantId, CharacterCreationData data) {
		if( handoffPlugin != null )  
		{  
			handoffPlugin.startingShipPicked(variantId, data);  
			return;  
		}  
		
		MutableCharacterStatsAPI stats = data.getPerson().getStats();
		stats.addAptitudePoints(1);
		stats.addSkillPoints(2);
		
		if (variantId.equals("vigilance_Starting")) {
			data.getStartingCargo().addFuel(20);
			data.getStartingCargo().addSupplies(30);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 20);
			data.getStartingCargo().addMarines(5);
		} else
		if (variantId.equals("lasher_Starting")) {
			data.getStartingCargo().addFuel(20);
			data.getStartingCargo().addSupplies(20);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 40);
			data.getStartingCargo().addMarines(10);
		} else
		if (variantId.equals("wolf_CS")) {
			data.getStartingCargo().addFuel(20);
			data.getStartingCargo().addSupplies(20);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 22);
			data.getStartingCargo().addMarines(7);
		} else
		if (variantId.equals("shuttle_Attack")) {
			data.getStartingCargo().addFuel(10);
			data.getStartingCargo().addSupplies(10);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 10);
			data.getStartingCargo().addMarines(3);
		} else				
		if (variantId.equals("hound_Assault")) {
			data.getStartingCargo().addFuel(30);
			data.getStartingCargo().addSupplies(40);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 25);
			data.getStartingCargo().addMarines(15);
		} else
		if (variantId.equals("tempest_Starting")) {
			data.getStartingCargo().addFuel(20);
			data.getStartingCargo().addSupplies(30);
			data.getStartingCargo().addCrew(CrewXPLevel.ELITE, 20);
			data.getStartingCargo().addMarines(0);
		} else {
			data.getStartingCargo().addFuel(20);
			data.getStartingCargo().addSupplies(20);
			data.getStartingCargo().addCrew(CrewXPLevel.REGULAR, 20);
			data.getStartingCargo().addMarines(10);
		}
	}
	
	public static boolean canBeLoaded(String fullyQualifiedClassName)  
	{  
		try  
		{  
			Global.getSettings().getScriptClassLoader().loadClass(fullyQualifiedClassName);  
			return true;  
		}  
		catch (ClassNotFoundException ex)  
		{  
			return false;  
		}        
	}
}







