	

    package data.scripts.plugins;
     
    import com.fs.starfarer.api.Global;
    import com.fs.starfarer.api.combat.CombatEngineAPI;
    import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
    import com.fs.starfarer.api.combat.ShipAPI;
    import java.lang.ref.WeakReference;
    import java.util.Collections;
    import java.util.Iterator;
    import java.util.List;
    import java.util.Set;
    //import java.util.WeakHashMap;
    import java.util.HashMap;
    import org.apache.log4j.Level;
     
    /* Original script by LazyWizard (I think)
    Modified (hit with a mallet) by Debido
    So what is this script? This script is a derivitive of the Incendiary script/
    It's purpose is to enable someone to activate the script, apply certain mutablestat effects to a ship
    After a given time the effects are un-applied to the ship. That is the high level.
     
    What happens at a psuedo-code level is this
     
    - The adrenaline_rush.java script is called when the player right-clicks or is activated by the AI.
    - The adrenaline_rush.java file has a single line that calls the startAdrenaline(float duration, shipAPI source) method in this class file
    - The startAdrenaline method does several things
    1. Applies the desired buffs/effects to the ship
    2. Creates a token in the juiced Hashmap
     
    - Conceptually a token is created but token has other connotations in Java so I'll clarify, but I will use the term token in the conceptual sense.
    - The 'token' created is a new AdrenalineData object with the public AdrenalineData(float adrenalineDuration, ShipAPI source) method
     
    - The important aspect of this token is that on creation it has a time value 'expiration' that is the current in-game time + adrenalineDuration in seconds that are passed to it from the shipsystem script startAdrenaline method..
    - Every frame of the game it will now check this script, iterate through the list of tokens in the juiced HashMap and determine if the current game time is equal to the time in the duration variable.
     
    - IF the game time is EQUAL to the time in the token, then it processes the token.
     
    - The processing of the token is thus:
    1. un-apply the mutable stats
    2. remove the token from the juiced hashmap
     
     
    Enjoy and don't have too much fun!
     
    */
     
     
    public class AdrenalineTracker implements EveryFrameCombatPlugin
    {
        // Stores the currently juiced juiceTokens
        // Having the Set backed by a WeakHashMap helps prevent memory leaks
        //private static final Set juiced = Collections.newSetFromMap(new WeakHashMap());
        private static final Set juiced = Collections.newSetFromMap(new HashMap());
     
            private static void applyAdrenalineStats(ShipAPI source)
            {
                String id = "steroids"; //id of the buff
                Global.getLogger(AdrenalineTracker.class).log(Level.INFO,"Running apply"); //debug code
               
                //Apply ship stats
                source.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, 100f);
                source.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, 6f);
                source.getMutableStats().getEmpDamageTakenMult().modifyPercent(id, - 70f);
                source.getMutableStats().getHighExplosiveDamageTakenMult().modifyPercent(id, - 70f);
                source.getMutableStats().getKineticDamageTakenMult().modifyPercent(id, - 70f);
                source.getMutableStats().getEnergyDamageTakenMult().modifyPercent(id, - 70f);
                source.getMutableStats().getFragmentationDamageTakenMult().modifyPercent(id, - 70f);
                source.getMutableStats().getCombatEngineRepairTimeMult().modifyMult(id, 0.5f);
                source.getMutableStats().getCombatWeaponRepairTimeMult().modifyMult(id, 0.3f);
                source.getMutableStats().getFluxDissipation().modifyPercent(id, 0f);
                source.getMutableStats().getAcceleration().modifyPercent(id,  30f);
                source.getMutableStats().getDeceleration().modifyPercent(id,  30f);
                source.getMutableStats().getTurnAcceleration().modifyPercent(id,  30f);
                source.getMutableStats().getMaxTurnRate().modifyPercent(id,  30f);
                source.getMutableStats().getMaxSpeed().modifyPercent(id,  30f);
                source.getMutableStats().getZeroFluxMinimumFluxLevel().modifyFlat(id,  1f);
       
            }
           
                    private static void unApplyAdrenalineStats(ShipAPI source)
            {
                String id = "steroids"; //id of the buff
                Global.getLogger(AdrenalineTracker.class).log(Level.INFO,"Running un-apply"); //debug code
               
                //unpply ship stats            
                source.getMutableStats().getMaxCombatHullRepairFraction().unmodify(id);
                source.getMutableStats().getHullCombatRepairRatePercentPerSecond().unmodify(id);
                source.getMutableStats().getEmpDamageTakenMult().unmodify(id);
                source.getMutableStats().getHighExplosiveDamageTakenMult().unmodify(id);
                source.getMutableStats().getKineticDamageTakenMult().unmodify(id);
                source.getMutableStats().getEnergyDamageTakenMult().unmodify(id);
                source.getMutableStats().getFragmentationDamageTakenMult().unmodify(id);
                source.getMutableStats().getCombatEngineRepairTimeMult().unmodify(id);
                source.getMutableStats().getCombatWeaponRepairTimeMult().unmodify(id);
                source.getMutableStats().getFluxDissipation().unmodify(id);
                source.getMutableStats().getAcceleration().unmodify(id);
                source.getMutableStats().getDeceleration().unmodify(id);
                source.getMutableStats().getTurnAcceleration().unmodify(id);
                source.getMutableStats().getMaxTurnRate().unmodify(id);
                source.getMutableStats().getMaxSpeed().unmodify(id); 
                source.getMutableStats().getZeroFluxMinimumFluxLevel().unmodify(id);     
            }
       
        private CombatEngineAPI engine;
     
     
        @Override
        public void advance(float amount, List events)
        {
            if (engine.isPaused() || juiced.isEmpty())
            {
                return;
            }
            // Deal juiceToken damage for all actively juiced projectiles
            for (Iterator iter = juiced.iterator(); iter.hasNext();)
            {
                AdrenalineData juiceToken = (AdrenalineData) iter.next();
     
                // Check if the juiceToken has gone out
                if (engine.getTotalElapsedTime(false) >= juiceToken.expiration)
                {
                    unApplyAdrenalineStats((ShipAPI)juiceToken.source.get());
                    //unApplyAdrenalineStats(juiceToken.source);
                    Global.getLogger(AdrenalineTracker.class).log(Level.INFO,"Running un-apply loop");
                    iter.remove();
                    Global.getLogger(AdrenalineTracker.class).log(Level.INFO,"Removed token");
                }
            }
        }
     
        public static void startAdrenaline(float adrenalineDuration, ShipAPI source)
        {
            //Creates a new token in the juiced list
            juiced.add(new AdrenalineData(adrenalineDuration, source));
            //Applies the mutablestats to the ship
            applyAdrenalineStats(source);
        }
     
        @Override
        public void init(CombatEngineAPI engine)
        {
            this.engine = engine;
            juiced.clear();
        }
     
        private static class AdrenalineData
        {
            public final WeakReference source;
            //public final ShipAPI source;
            private final float expiration;
     
            public AdrenalineData(float adrenalineDuration, ShipAPI source)
            {
                //this.hitLoc = new AnchoredEntity(target, hitLoc);
                this.source = new WeakReference(source);
                //this.source = source;
                //sets the time WHEN it will expire
                expiration = Global.getCombatEngine().getTotalElapsedTime(false)
                        + adrenalineDuration;
            }        
        }
    }

