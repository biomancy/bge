package data.scripts.plugins;


import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
import data.scripts.weapons.DissableShieldOnHitEffect;
import java.util.List;

public class ForceShieldDissablePlugin implements EveryFrameCombatPlugin
{
    @Override
    public void advance(float amount, List events) {
        DissableShieldOnHitEffect.forceDissableShields(amount);
    }

    @Override
    public void init(CombatEngineAPI engine) { }
}