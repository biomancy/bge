package data.scripts.weapons;  
  
import com.fs.starfarer.api.Global;  
import com.fs.starfarer.api.combat.CombatEngineAPI;  
import com.fs.starfarer.api.combat.CombatEntityAPI;  
import com.fs.starfarer.api.combat.DamagingProjectileAPI;  
import com.fs.starfarer.api.combat.OnHitEffectPlugin;  
import com.fs.starfarer.api.combat.ShipAPI;  
import org.lwjgl.util.vector.Vector2f;  
import org.apache.log4j.Level;  
import data.scripts.plugins.ProtectionData;  
  
public class LeechShotOnHitEffect implements OnHitEffectPlugin  
{  
  
    private float sourceHealth;  
    private float sourceHitpoints;  
    //set the below multiplier to change the proportion of the weapon_data.csv file damage effect.  
    private final float damageMultiplier = 0.15f;  
  
    @Override  
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine)  
    {  
        //get the projectile source  
        ShipAPI source = projectile.getSource();  
          
        //get the amount of damage done by the projectile  
        float weaponDamage = projectile.getDamageAmount();  
        //apply the damage multiplier  
        float finalDamage = weaponDamage * damageMultiplier;  
               
          
          
        // Check if it's a valid target, check if it's a ship, check that the projectile didDamage and lastly check if the shield is off.  
        if (target != null && target instanceof ShipAPI && projectile.didDamage() /*&& !shieldHit*/)  
        {  
                //get the hull level of the source ship as a %  
                sourceHealth = source.getHullLevel();  
                //get the current hitpoints of the source ship  
                sourceHitpoints = source.getHitpoints();  
                  
                //if the health of the source ship is less then 100% of normal  
                if (sourceHealth < 1f) {  
                      
                    //check if the leech effect would put the ship over it's Maximum Hitpoints aka health      
                    if ((sourceHitpoints + finalDamage) >= source.getMaxHitpoints()) {  
                        // then set the hitpoints to the maximum hitpoints  
                        source.setHitpoints(source.getMaxHitpoints());  
                          
                        //else if applying final damage would take the ship over 100%, perform as normal. This else if step can be made else after debug is removed.  
                    } else if ((sourceHitpoints + finalDamage) < source.getMaxHitpoints()) {  
  
                    //Give the amount of damage applied per second to the hit points of the source ship  
                       
                    ((ShipAPI) source).setHitpoints(sourceHitpoints + finalDamage);  
                                          
                    //debug, remove for final  
                    Global.getLogger(LeechShotOnHitEffect.class).log(Level.INFO,"Leech shot healed source: " + finalDamage);  
                      
                } else {  
                    //debug, remove for final  
                    Global.getLogger(LeechShotOnHitEffect.class).log(Level.INFO,"Leech shot didn't heal source");  
                }  
        }  
    }  
          
       if (target != null && target instanceof ShipAPI   
               && weaponDamage >= target.getHitpoints())  
       {  
          ProtectionData.startProtection(1f, source);  
       }  
    }  
} 