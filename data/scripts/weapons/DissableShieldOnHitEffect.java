package data.scripts.weapons;


import com.fs.starfarer.api.Global;

import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.CombatEngineAPI;

import com.fs.starfarer.api.combat.CombatEntityAPI;

import com.fs.starfarer.api.combat.DamagingProjectileAPI;

import com.fs.starfarer.api.combat.OnHitEffectPlugin;

import com.fs.starfarer.api.combat.ShipAPI;

import data.scripts.plugins.ProtectionData;  
import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;



public class DissableShieldOnHitEffect implements OnHitEffectPlugin 
{
    
// This is how we store which ships have dissabled shields and how much
    
//  longer they'll be dissabled.
    //
    // Key - ShipAPI (the affected ship)
    
// Value - float (the remaining duration of shield dissable-ization)
    
static Map affectedShips = new HashMap();

    static final float DISSABLE_TIME_PER_HIT = 0.8f; 
// In seconds

    
@Override
	
public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) 
{
        
            ShipAPI source = projectile.getSource();
            float weaponDamage = projectile.getDamageAmount(); 

       if (target != null && target instanceof ShipAPI   
               && weaponDamage >= target.getHitpoints())  
       {  
          ProtectionData.startProtection(1f, source);  
       }   



if (!(target instanceof ShipAPI)) return;

        
ShipAPI ship = (ShipAPI)target;

        
if(!ship.isAlive() || ship.getShield() == null) return;

        
if(affectedShips.containsKey(ship)) 
{
            
affectedShips.put(ship, (Float)affectedShips.get(ship) + DISSABLE_TIME_PER_HIT);
        
} 
else 
{
            
affectedShips.put(ship, DISSABLE_TIME_PER_HIT);
        
}
	
}

	
// This is called once each frame by the ForceShieldDissablePlugin
    
public static void forceDissableShields(float amount) 
{
        
CombatEngineAPI engine = Global.getCombatEngine();

        
if(engine.isPaused()) return;

        
for(Iterator iter = affectedShips.keySet().iterator(); 
iter.hasNext();) 
{
            
ShipAPI ship = (ShipAPI)iter.next();
            
float remainingTime = (Float)affectedShips.get(ship) - amount;

            
if(remainingTime < 0) 
{
                
// This removes the current ship from affectedShips
                
iter.remove();
            
} 
else 
{
                
affectedShips.put(ship, remainingTime);
                
ship.getShield().setActiveArc(0);
            }
        }
    }
}