package data.shipsystems.scripts;


import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;

public class OnOnceAI implements ShipSystemAIScript {

    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipSystemAPI system;
    private final IntervalUtil tracker = new IntervalUtil(1.0f, 1.5f);
    private int shouldUseSystem = 9000;
	
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) 
    {
        if(engine == null) return;
        if(engine.isPaused()) return;
        //if(!ship.getSystem().isOn()) shouldUseSystem = true;       
        tracker.advance(amount);
        if (tracker.intervalElapsed() && ship.getDeployedDrones().isEmpty() && shouldUseSystem == 9000) 
        {        
            shouldUseSystem = 0;
        }
        if(tracker.intervalElapsed() && shouldUseSystem < 2){
            ship.giveCommand(ShipCommand.USE_SYSTEM,null,0);
            shouldUseSystem = shouldUseSystem + 1;
        }
    }
}
