package data.shipsystems.scripts;


import com.fs.starfarer.api.combat.MutableShipStatsAPI;

import com.fs.starfarer.api.plugins.ShipSystemStatsScript;


import com.fs.starfarer.api.Global;

import com.fs.starfarer.api.combat.ShipAPI;

import org.lazywizard.lazylib.MathUtils;

import java.util.Iterator;

import java.util.HashMap;

import java.util.Map;



public class hyperspace_web implements ShipSystemStatsScript 
{
    
        
public static final float RANGE = 1200f;
	
public static final float ACCURACY_BONUS = -30f;
	
public static final float RANGE_BONUS = -15f;
        
        
private static Map jamming = new HashMap();
        
        

@Override
	

public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) 
{
            
//Declares two objects of type ShipAPI. 'ship' is just a generic holder for ships that are cycled through. 'host_ship' is the ship that is using the system.	
            
ShipAPI ship;	
            
ShipAPI host_ship = (ShipAPI) stats.getEntity();	
            
            
for (Iterator iter = Global.getCombatEngine().getShips().iterator(); iter.hasNext();)
            
{
                
ship = (ShipAPI) iter.next(); 
//Loads the current ship the iterator is on into 'ship'

		if (ship.isHulk()) continue; 
//We don't want to bother modifying stats of the ship if it's disabled.
                
if (ship == host_ship) continue; //Doesn't let the host ship receive the benefits it's giving to others.  Probably redundant.
			
		
//If the ship is on the same team as the host ship, and it's within range, and its a fighter...
		
if ((host_ship.getOwner() != ship.getOwner()) && (MathUtils.getDistance(ship, host_ship) <= (RANGE)))  
{
				
		
//Modify this ship's stats.
		
ship.getMutableStats().getMaxSpeed().modifyPercent(id, ACCURACY_BONUS);

ship.getMutableStats().getAcceleration().modifyPercent(id, ACCURACY_BONUS);
ship.getMutableStats().getMaxTurnRate().modifyPercent(id, RANGE_BONUS);

ship.getMutableStats().getTurnAcceleration().modifyPercent(id, RANGE_BONUS);		
ship.getMutableStats().getBeamWeaponDamageMult().modifyPercent(id, RANGE_BONUS);	
		
ship.getMutableStats().getEnergyWeaponDamageMult().modifyPercent(id, RANGE_BONUS);

ship.getMutableStats().getBallisticWeaponDamageMult().modifyPercent(id, RANGE_BONUS);	
		
ship.getMutableStats().getMissileWeaponDamageMult().modifyPercent(id, RANGE_BONUS);
						
		
//Adds the ship to the hashmap, and associates it with the host ship.
		
jamming.put(ship, host_ship);
                System.out.println();
			
                
//If the ship isn't in range but is contained in the hashmap, and the host ship of the ship is indeed this one...
		
} 
else if ((jamming.containsKey(ship)) && (jamming.get(ship) == host_ship))
{

		
//removes all benefits
		
ship.getMutableStats().getMaxSpeed().unmodify(id);	
		
ship.getMutableStats().getAcceleration().unmodify(id);	
		
ship.getMutableStats().getMaxTurnRate().unmodify(id);

ship.getMutableStats().getTurnAcceleration().unmodify(id);	
		
ship.getMutableStats().getBeamWeaponDamageMult().unmodify(id);	
		
ship.getMutableStats().getEnergyWeaponDamageMult().unmodify(id);

ship.getMutableStats().getBallisticWeaponDamageMult().unmodify(id);	
		
ship.getMutableStats().getMissileWeaponDamageMult().unmodify(id);
					
		
//Removes the ship from the hashmap.
		jamming.remove(ship);				
		
}        
            }
        }   
        
        
@Override
	public void unapply(MutableShipStatsAPI stats, String id) 
{

		
//Removes the effects from the host ship.
		
stats.getMaxSpeed().unmodify(id);
		
stats.getMaxTurnRate().unmodify(id);
		
stats.getTurnAcceleration().unmodify(id);
		
stats.getAcceleration().unmodify(id);
		
stats.getZeroFluxMinimumFluxLevel().unmodify(id);	
		
		
//same objects as before.
		
ShipAPI ship;	
		
ShipAPI host_ship  = (ShipAPI) stats.getEntity();
		
System.out.println();
		
//Loops through all the ships in the hashmap.
            
for (Iterator iter = jamming.keySet().iterator(); iter.hasNext();)
            
{
                
ship = (ShipAPI) iter.next();
		
			
//If the ship in the hash map is receiving benefits from this host ship (which is currently powering-down its system):
			
//(This makes it so that one host ship bringing down its system doesn't remove benefits that are being applied to other ships by host ships elsewhere.
			
if (jamming.get(ship) == host_ship) {
			
				
//removes all benefits
				
ship.getMutableStats().getMaxSpeed().unmodify(id);	
		
ship.getMutableStats().getAcceleration().unmodify(id);	
		
ship.getMutableStats().getMaxTurnRate().unmodify(id);

ship.getMutableStats().getTurnAcceleration().unmodify(id);	
		
ship.getMutableStats().getBeamWeaponDamageMult().unmodify(id);	
		
ship.getMutableStats().getEnergyWeaponDamageMult().unmodify(id);

ship.getMutableStats().getBallisticWeaponDamageMult().unmodify(id);	
		
ship.getMutableStats().getMissileWeaponDamageMult().unmodify(id);			
}	
            }
	}
	
        
@Override
	
public StatusData getStatusData(int index, State state, float effectLevel) 
{

		
if (index == 0) 
{
			
return new StatusData("active", false);
		
}
		return null;
	}
}
