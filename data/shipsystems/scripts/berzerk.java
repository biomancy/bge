	

    package data.shipsystems.scripts;
     
    import com.fs.starfarer.api.Global;
    import com.fs.starfarer.api.combat.MutableShipStatsAPI;
    import com.fs.starfarer.api.combat.ShipAPI;
    import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
    import com.fs.starfarer.api.combat.WeaponAPI;
    import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
    import java.util.Iterator;
    import org.apache.log4j.Level;
     
     
     
    public class berzerk implements ShipSystemStatsScript {
       
        private boolean disableTrigger = true;
        private ShipAPI ship;
     
     
            public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
                ship = (ShipAPI) stats.getEntity();
               
                if (state == state.ACTIVE) {
                    disableTrigger = false;
                }
     
               
                if (state == ShipSystemStatsScript.State.OUT) {
     // to slow down ship to its regular top speed while powering drive down
                            stats.getMaxTurnRate().unmodify(id);
                    } else {
     
                            float modifier = 30f / (float)Math.max(0.3f, ship.getHullLevel());
                            float modifiermult = (float)Math.max(0.3f, ship.getHullLevel() / 2f);
     
                           

                            stats.getFluxDissipation().modifyPercent(id, 1.5f * modifier * effectLevel);
                            stats.getMissileRoFMult().modifyPercent(id,  modifier * effectLevel);
                            stats.getBallisticRoFMult().modifyPercent(id,  modifier * effectLevel);
                            stats.getEnergyRoFMult().modifyPercent(id,  modifier * effectLevel);
                            stats.getHullDamageTakenMult().modifyMult(id, modifiermult * effectLevel);
                            stats.getArmorDamageTakenMult().modifyMult(id, modifiermult * effectLevel);

     
                            stats.getAcceleration().modifyPercent(id,  modifier * effectLevel);
                            stats.getDeceleration().modifyPercent(id,  modifier * effectLevel);
                            stats.getTurnAcceleration().modifyPercent(id,  modifier * effectLevel);
                            stats.getMaxTurnRate().modifyPercent(id,  modifier * effectLevel);
                            stats.getMaxSpeed().modifyPercent(id,  modifier * effectLevel);
     
     
     
     
     
                    }
            }
            public void unapply(MutableShipStatsAPI stats, String id) {

                            stats.getFluxDissipation().unmodify(id);
                            stats.getBallisticWeaponFluxCostMod().unmodify(id);
                            stats.getMissileWeaponFluxCostMod().unmodify(id);
                            stats.getMissileRoFMult().unmodify(id);
                            stats.getBallisticRoFMult().unmodify(id);
                            stats.getEnergyRoFMult().unmodify(id);
                            stats.getHullBonus().unmodify(id);    
     
     
                            stats.getAcceleration().unmodify(id);
                            stats.getDeceleration().unmodify(id);
                            stats.getTurnAcceleration().unmodify(id);
                            stats.getMaxTurnRate().unmodify(id);
                            stats.getMaxSpeed().unmodify(id);
                            //Global.getLogger(berzerk.class).log(Level.INFO,disableTrigger);
                            if (!disableTrigger)
                            {
     
                            //Global.getLogger(berzerk.class).log(Level.INFO,"Running disabler"); //debug code
                            for (Iterator iter = ship.getAllWeapons().iterator(); iter.hasNext();)
                            {
                                WeaponAPI weapon = (WeaponAPI) iter.next();
                                if (weapon.getType() != weapon.getType().DECORATIVE)
                                {
                                   weapon.disable();
                                }
                               
                            }
                           
                            for (Iterator iter = ship.getEngineController().getShipEngines().iterator(); iter.hasNext();)
                            {
                                ShipEngineAPI engine = (ShipEngineAPI) iter.next();
                                engine.disable();
                            }
                           
     
                            disableTrigger = true;
                               
                            }
                           
     
                           
                           
                           
            }
           
            public StatusData getStatusData(int index, State state, float effectLevel) {
                    if (index == 1) {
                            return new StatusData("Blood Lust!!!!", false);
                    }
                    return null;
    //              if (index == 0) {
    //                      return new StatusData("improved maneuverability", false);
    //              } else if (index == 1) {
    //                      return new StatusData("+50 top speed", false);
    //              }
    //              return null;
    //              if (index == 0) {
    //                      return new StatusData("improved maneuverability", false);
    //              }
    //              if (index == 1) {
    //                      return new StatusData("improved maneuverability", false);
    //              }
    //              if (index == 2) {
    //                      return new StatusData("+50 top speed", false);
    //              }
    //              return null;
            }
    }

