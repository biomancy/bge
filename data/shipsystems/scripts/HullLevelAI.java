package data.shipsystems.scripts;

import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.*;

public class HullLevelAI implements ShipSystemAIScript {

	private ShipAPI ship;
	private CombatEngineAPI engine;
	private ShipwideAIFlags flags;
	private ShipSystemAPI system;
	
	private float trackerPlus = 0.0f;
	private boolean shouldUseSystem = false;
	private IntervalUtil tracker = new IntervalUtil(0.35f + trackerPlus, 0.6f + trackerPlus);
	
	public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
		this.ship = ship;
		this.flags = flags;
		this.engine = engine;
		this.system = system;
	}

	@SuppressWarnings("unchecked")
	public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
		tracker.advance(amount);
		if (tracker.intervalElapsed()) {


			float level = ship.getHullLevel();
			if (level > 0.45f)//We're probably heading towards overload, start it up.
			{
				shouldUseSystem = true;
			}
			if (level < 0.7f)//We're probably heading towards overload, shut it down!
			{
				shouldUseSystem = false;
			}	
			if (level > 0.2f)//Shut down when we've lost all Hard Flux.
			{
				shouldUseSystem = false;
			}			
			
            // If system is inactive and should be active, enable it
            // If system is active and shouldn't be, disable it
            if (ship.getSystem().isActive() == true && shouldUseSystem == false)
            {
                ship.useSystem();
				trackerPlus = 0.0f;
            }	
            if (ship.getSystem().isActive() == true && shouldUseSystem == true)
            {
				trackerPlus = 0.0f;
            }
            if (ship.getSystem().isActive() == false && shouldUseSystem == false)
            {
				trackerPlus = 0.0f;
            }			
			if(ship.getSystem().isActive() == false && shouldUseSystem == true)
			{
                ship.useSystem();
				trackerPlus = 5.0f;//Wait a minimum of 5 seconds after this to turn it off!
            }			
		}
	}
}
